package mypackage.net;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.io.Connection;
import javax.microedition.io.HttpConnection;

import net.rim.device.api.io.transport.ConnectionDescriptor;
import net.rim.device.api.io.transport.ConnectionFactory;
import net.rim.device.api.io.transport.TransportInfo;
import net.rim.device.api.ui.component.Dialog;

import mypackage.Utilities;
import mypackage.ui.MyScreen;

public class MyConnector implements Runnable {

	String full_url;
	MyScreen myScreen;

	public MyConnector(String full_url, MyScreen myScreen) {
		// TODO Auto-generated constructor stub
		this.full_url = full_url;
		this.myScreen = myScreen;
	}

	public void run() {
		// TODO Auto-generated method stub
		// Dialog.inform("Thread started with " + full_url);
		String htmlPage = null;
		try {
			htmlPage = connectAndReturnThePage(full_url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		myScreen.setHtml(htmlPage);
		
	}

	private String readResponseAsString(InputStream input) throws IOException {
		String html;
		byte[] buffer = new byte[256];
		int len = 0;
		int size = 0;
		StringBuffer raw = new StringBuffer();
		while (-1 != (len = input.read(buffer))) {
			raw.append(new String(buffer, 0, len));
			size += len;
		}
		raw.insert(0, "bytes received]\n");
		raw.insert(0, size);
		raw.insert(0, '[');
		html = raw.toString();
		return html;
	}

	private String connectAndReturnThePage(String url) throws IOException {
		int status = 0;
		HttpConnection http_conn = null;
		String html = null;
		// 1. Get a suitable connection object and execute the request
		ConnectionFactory connection_factory = new ConnectionFactory();
		int[] preferred_transports = { TransportInfo.TRANSPORT_MDS };
		connection_factory.setPreferredTransportTypes(preferred_transports);
		ConnectionDescriptor conn_desc = connection_factory.getConnection(url);
		Connection connection = conn_desc.getConnection();
		http_conn = (HttpConnection) connection;
		// 2. Check the result
		status = http_conn.getResponseCode();
		if (status == HttpConnection.HTTP_OK) {
			// success. So now read back the results
			InputStream input;
			input = http_conn.openInputStream();
			html = readResponseAsString(input);
			input.close();
		} else {
			html = "response code = " + status;
		}
		return html;
	}

}
