package mypackage.ui;

import mypackage.net.MyConnector;
import net.rim.device.api.command.Command;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.util.StringProvider;

/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class MyScreen extends MainScreen
{
	// initialise fields
	private EditField web_address_field = new EditField("URL: ", "http://");
	private ButtonField send_button = new ButtonField("Send!", Field.FIELD_HCENTER | ButtonField.CONSUME_CLICK);
	private EditField html = new EditField("","");
	
	UiApplication currentUiApp = UiApplication.getUiApplication();
	
	private FieldChangeListener sent_listener = new FieldChangeListener(){

		public void fieldChanged(Field field, int context) {
			String full_url = web_address_field.getText();
			if (!full_url.startsWith("http://"))
				full_url = "http://" + full_url;
			
			synchronized (UiApplication.getEventLock()) {
				// Start new connection thread
				MyConnector newConnection = new MyConnector(full_url, MyScreen.this);
				Thread runner = new Thread(newConnection);
				runner.start();
			}
		}
	};
	
	private MenuItem mi_render = new MenuItem(new StringProvider("Render HTML"),10, 10);
	private LabelField renderPage = new LabelField("Render Page");
	
    /**
     * Creates a new MyScreen object
     */
    public MyScreen()
    {        
        // Set the displayed title of the screen       
        setTitle("Tomek's net app");
        add(web_address_field);
        
        send_button.setChangeListener(sent_listener);
        add(send_button);
        add(html);
        
        mi_render.setCommandContext(new LabelField("Rendering HTML"));
        mi_render.setCommand(new Command(new RenderMenuCommandHandler(html)));
		addMenuItem(mi_render);
    }
    
    protected boolean onSavePrompt() {
    	return true;
    }
    
    public void setHtml(String the_html){
    	synchronized (UiApplication.getEventLock()) {
    		html.setText(the_html);
    	}
    }
}
