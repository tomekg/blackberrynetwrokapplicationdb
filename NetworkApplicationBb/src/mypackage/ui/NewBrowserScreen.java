package mypackage.ui;

import net.rim.device.api.browser.field2.BrowserField;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.container.MainScreen;

public class NewBrowserScreen extends MainScreen {
	UiApplication currentUiApp = UiApplication.getUiApplication();

//	public NewBrowserScreen() {
//		// TODO Auto-generated constructor stub
//
//		// create new instance of the BrowserField
//		BrowserField browserField = new BrowserField();
//
//		// add the browser field to a ui manager or screen
//		add(browserField);
//
//		// request the content you wish to display
//		// this method call is typically called once
//		browserField.requestContent("http://blackberry.com");
//
//		currentUiApp.pushScreen(this);
//	}

	public NewBrowserScreen(EditField html) {

		// create new instance of the BrowserField
		BrowserField browserField = new BrowserField();

		// request the content you wish to display
		// this method call is typically called once
		browserField.displayContent(html.getText(), "www.google.com");
		
		// add the browser field to a ui manager or screen
		add(browserField);

		currentUiApp.pushScreen(this);
	}

}
