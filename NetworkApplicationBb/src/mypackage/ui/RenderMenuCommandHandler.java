package mypackage.ui;

import net.rim.device.api.command.CommandHandler;
import net.rim.device.api.command.ReadOnlyCommandMetadata;
import net.rim.device.api.ui.component.EditField;

public class RenderMenuCommandHandler extends CommandHandler {

	EditField html;
	
	public RenderMenuCommandHandler(EditField html) {
		this.html = html;
	}

	public void execute(ReadOnlyCommandMetadata arg0, Object arg1) {
		// create new screen 
        NewBrowserScreen newBrowserScreen = new NewBrowserScreen(html);
	}

}
